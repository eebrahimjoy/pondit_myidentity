import 'package:flutter/material.dart';

void main() {
  runApp(
    MyIdentity(),
  );
}

class MyIdentity extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.grey,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Image.asset("assets/images/profile.png"),
              ),
              Text(
                "David Waarner",
                style: TextStyle(
                    fontFamily: "Pacifico",
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                "SOFTWARE ENGINEER",
                style: TextStyle(
                    fontFamily: "SourceSansPro",
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 3,
                    color: Colors.grey.shade900),
              ),
              SizedBox(
                height: 8,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/images/apple.png",
                    height: 24,
                    width: 24,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Image.asset(
                    "assets/images/facebook.png",
                    height: 24,
                    width: 24,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Image.asset(
                    "assets/images/google.png",
                    height: 24,
                    width: 24,
                  ),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                child: Text(
                  "I am highly positive thinker, good team leader, can work under pressure & also can adobt new technology",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: "SourceSansPro",
                      fontSize: 14,
                      fontWeight: FontWeight.normal,
                      color: Colors.black),
                ),
              ),
              Card(
                color: Colors.teal,
                margin:
                    EdgeInsets.only(top: 8.0, bottom: 8.0, left: 24, right: 24),
                child: ListTile(
                  leading: Icon(
                    Icons.phone,
                    color: Colors.white,
                  ),
                  title: Text(
                    "+88 01914803915",
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: "SourceSansPro",
                        color: Colors.white),
                  ),
                ),
              ),
              Card(
                color: Colors.teal,
                margin:
                    EdgeInsets.only(top: 8.0, bottom: 8.0, left: 24, right: 24),
                child: ListTile(
                  leading: Icon(
                    Icons.email,
                    color: Colors.white,
                  ),
                  title: Text(
                    "eebrahimjoy@gmail.com",
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: "SourceSansPro",
                        color: Colors.white),
                  ),
                ),
              ),
              ElevatedButton.icon(
                onPressed: () {},
                style: ElevatedButton.styleFrom(primary: Colors.black),
                icon: Icon(Icons.add_task_outlined,color: Colors.white,),
                label: Text("Hire me now",style: TextStyle(
                  color: Colors.white
                ),),

              ),
            ],
          ),
        ),
      ),
    );
  }
}
